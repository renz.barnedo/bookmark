import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DownloadComponent } from './download.component';
import { DefaultButtonModule } from 'src/app/widgets/default-button/default-button.module';
import { BrowserCardModule } from 'src/app/widgets/browser-card/browser-card.module';

@NgModule({
  declarations: [DownloadComponent],
  imports: [CommonModule, BrowserCardModule],
  exports: [DownloadComponent],
})
export class DownloadModule {}
