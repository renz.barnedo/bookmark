import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterComponent } from './footer.component';
import { LogoModule } from 'src/app/widgets/logo/logo.module';
import { NavLinksModule } from 'src/app/widgets/nav-links/nav-links.module';
import { SocialIconsModule } from 'src/app/widgets/social-icons/social-icons.module';

@NgModule({
  declarations: [FooterComponent],
  imports: [CommonModule, LogoModule, NavLinksModule, SocialIconsModule],
  exports: [FooterComponent],
})
export class FooterModule {}
