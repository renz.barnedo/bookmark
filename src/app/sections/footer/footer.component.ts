import { Component, OnInit } from '@angular/core';
import { SharedService } from 'src/app/widgets/shared.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {
  isMobile: boolean = false;

  constructor(private sharedService: SharedService) {}

  ngOnInit(): void {}
}
