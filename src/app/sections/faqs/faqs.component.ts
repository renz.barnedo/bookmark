import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-faqs',
  templateUrl: './faqs.component.html',
  styleUrls: ['./faqs.component.scss'],
})
export class FaqsComponent implements OnInit {
  questions = [
    {
      question: 'What is Bookmark?',
      answer:
        '  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt  justo eget ultricies fringilla. Phasellus blandit ipsum quis quam ornare mattis.',
      isOpened: false,
      isClicked: false,
    },
    {
      question: 'How can I request a new browser?',
      answer:
        '   Vivamus luctus eros aliquet convallis ultricies. Mauris augue massa, ultricies non ligula. Suspendisse imperdiet. Vivamus luctus eros aliquet convallis ultricies. Mauris augue massa, ultricies non ligula. Suspendisse imperdie tVivamus luctus eros aliquet convallis ultricies. Mauris augue massa, ultricies non ligula. Suspendisse imperdiet.',
      isOpened: false,
      isClicked: false,
    },
    {
      question: 'Is there a mobile app?',
      answer:
        'Sed consectetur quam id neque fermentum accumsan. Praesent luctus vestibulum dolor, ut condimentum urna vulputate eget. Cras in ligula quis est pharetra mattis sit amet pharetra purus. Sed sollicitudin ex et ultricies bibendum.',
      isOpened: false,
      isClicked: false,
    },
    {
      question: 'What about other Chromium browsers?',
      answer:
        'Integer condimentum ipsum id imperdiet finibus. Vivamus in placerat mi, at euismod dui. Aliquam vitae neque eget nisl gravida pellentesque non ut velit.',
      isOpened: false,
      isClicked: false,
    },
  ];

  constructor() {}

  ngOnInit(): void {}

  setOpenedPanel(index: number) {
    if (!this.questions[index].isClicked) {
      this.questions[index].isClicked = true;
    }
    this.questions[index].isOpened = !this.questions[index].isOpened;
  }
}
