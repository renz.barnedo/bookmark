import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FaqsComponent } from './faqs.component';
import { DefaultButtonModule } from 'src/app/widgets/default-button/default-button.module';
import { IconModule } from 'src/app/widgets/icon/icon.module';

@NgModule({
  declarations: [FaqsComponent],
  imports: [CommonModule, DefaultButtonModule, IconModule],
  exports: [FaqsComponent],
})
export class FaqsModule {}
