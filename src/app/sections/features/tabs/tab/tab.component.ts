import { Component, Input, OnInit } from '@angular/core';
import { Icon } from 'src/app/widgets/icon/icon.component';

@Component({
  selector: 'app-tab',
  templateUrl: './tab.component.html',
  styleUrls: ['./tab.component.scss'],
})
export class TabComponent implements OnInit {
  @Input() heading = 'Bookmark in one click';
  @Input() description =
    'Organize your bookmarks however you like. Our simple drag-and-drop interface gives you complete control over how you manage your favourite sites.';
  @Input() link = '#';
  @Input() imageName: Icon = 'illustration-features-tab-1';

  constructor() {}

  ngOnInit(): void {}
}
