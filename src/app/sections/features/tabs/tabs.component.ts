import { Component, OnInit } from '@angular/core';
import { Icon } from 'src/app/widgets/icon/icon.component';

type Tab = {
  heading: string;
  description: string;
  link: string;
  imageName: Icon;
};

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss'],
})
export class TabsComponent implements OnInit {
  tabs: Tab[] = [
    {
      heading: 'Bookmark in one click',
      description:
        'Organize your bookmarks however you like. Our simple drag-and-drop interface gives you complete control over how you manage your favourite sites.',
      link: '#',
      imageName: 'illustration-features-tab-1',
    },
    {
      heading: 'Intelligent search',
      description:
        ' Our powerful search feature will help you find saved sites in no time at all. No need to trawl through all of your bookmarks.',
      link: '#',
      imageName: 'illustration-features-tab-2',
    },
    {
      heading: 'Share your bookmarks',
      description:
        ' Easily share your bookmarks and collections with others. Create a shareable link that you can send at the click of a button.',
      link: '#',
      imageName: 'illustration-features-tab-3',
    },
  ];
  activeTabIndex = 0;
  toBeInactiveTabIndex = 0;

  constructor() {}

  ngOnInit(): void {}

  setActiveTabIndex(index: number) {
    this.toBeInactiveTabIndex = this.activeTabIndex;
    this.activeTabIndex = index;
  }

  setTabClass(index: number) {
    if (index === this.activeTabIndex) {
      if (this.activeTabIndex > this.toBeInactiveTabIndex) {
        return 'tab-active show-from-right';
      } else if (this.activeTabIndex < this.toBeInactiveTabIndex) {
        return 'tab-active show-from-left';
      }
      return 'tab-active';
    }
    if (index === this.toBeInactiveTabIndex) {
      if (this.toBeInactiveTabIndex < this.activeTabIndex) {
        return 'tab-inactive hide-to-left';
      } else if (this.toBeInactiveTabIndex > this.activeTabIndex) {
        return 'tab-inactive hide-to-right';
      }
      return 'tab-inactive';
    }
    return 'tab-inactive';
  }
}
