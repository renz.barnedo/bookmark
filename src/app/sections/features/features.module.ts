import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FeaturesComponent } from './features.component';
import { IllustrationModule } from 'src/app/widgets/illustration/illustration.module';
import { TabsComponent } from './tabs/tabs.component';
import { TabComponent } from './tabs/tab/tab.component';
import { DefaultButtonModule } from 'src/app/widgets/default-button/default-button.module';

@NgModule({
  declarations: [FeaturesComponent, TabsComponent, TabComponent],
  imports: [CommonModule, IllustrationModule, DefaultButtonModule],
  exports: [FeaturesComponent],
})
export class FeaturesModule {}
