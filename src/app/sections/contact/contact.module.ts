import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactComponent } from './contact.component';
import { IconModule } from 'src/app/widgets/icon/icon.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [ContactComponent],
  imports: [CommonModule, IconModule, ReactiveFormsModule],
  exports: [ContactComponent],
})
export class ContactModule {}
