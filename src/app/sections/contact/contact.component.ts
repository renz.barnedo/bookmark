import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  ValidationErrors,
  Validators,
} from '@angular/forms';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],
})
export class ContactComponent implements OnInit {
  contactForm: FormGroup = this.fb.group({
    emailAddress: this.fb.control('', [Validators.required, Validators.email]),
  });
  errors: ValidationErrors | null = null;

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {}

  checkErrors() {
    if (this.contactForm.invalid) {
      this.errors = this.contactForm.controls.emailAddress.errors;
      return this.errors;
    }
    this.errors = null;
    return this.errors;
  }

  onContactFormSubmit() {
    if (this.checkErrors()) {
      return;
    }
  }
}
