import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeroComponent } from './hero.component';
import { NavbarModule } from 'src/app/widgets/navbar/navbar.module';
import { IllustrationModule } from 'src/app/widgets/illustration/illustration.module';

@NgModule({
  declarations: [HeroComponent],
  imports: [CommonModule, NavbarModule, IllustrationModule],
  exports: [HeroComponent],
})
export class HeroModule {}
