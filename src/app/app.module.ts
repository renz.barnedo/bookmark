import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { ContactModule } from './sections/contact/contact.module';
import { DownloadModule } from './sections/download/download.module';
import { FaqsModule } from './sections/faqs/faqs.module';
import { FeaturesModule } from './sections/features/features.module';
import { FooterModule } from './sections/footer/footer.module';
import { HeroModule } from './sections/hero/hero.module';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    HeroModule,
    FeaturesModule,
    DownloadModule,
    FaqsModule,
    ContactModule,
    FooterModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
