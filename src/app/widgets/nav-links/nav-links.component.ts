import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-nav-links',
  templateUrl: './nav-links.component.html',
  styleUrls: ['./nav-links.component.scss'],
})
export class NavLinksComponent implements OnInit {
  @Input() isLight = false;
  @Input() showLogin = true;
  @Input() gap = '1.6rem';
  @Input() inMobileMenu = false;

  constructor() {}

  ngOnInit(): void {}
}
