import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavLinksComponent } from './nav-links.component';

@NgModule({
  declarations: [NavLinksComponent],
  imports: [CommonModule],
  exports: [NavLinksComponent],
})
export class NavLinksModule {}
