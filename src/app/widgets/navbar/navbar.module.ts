import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar.component';
import { LogoModule } from '../logo/logo.module';
import { NavLinksModule } from '../nav-links/nav-links.module';
import { IconModule } from '../icon/icon.module';
import { SocialIconsModule } from '../social-icons/social-icons.module';

@NgModule({
  declarations: [NavbarComponent],
  imports: [
    CommonModule,
    LogoModule,
    NavLinksModule,
    IconModule,
    IconModule,
    SocialIconsModule,
  ],
  exports: [NavbarComponent],
})
export class NavbarModule {}
