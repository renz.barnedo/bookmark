import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
  menuIsOpen = false;
  hasClicked = false;

  constructor() {}

  ngOnInit(): void {}

  openMenu(opened: boolean) {
    if (!this.hasClicked) {
      this.hasClicked = true;
    }
    this.menuIsOpen = opened;
  }
}
