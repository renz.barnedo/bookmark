import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserCardComponent } from './browser-card.component';
import { DefaultButtonModule } from '../default-button/default-button.module';
import { IconModule } from '../icon/icon.module';

@NgModule({
  declarations: [BrowserCardComponent],
  imports: [CommonModule, DefaultButtonModule, IconModule],
  exports: [BrowserCardComponent],
})
export class BrowserCardModule {}
