import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BrowserCardComponent } from './browser-card.component';

describe('BrowserCardComponent', () => {
  let component: BrowserCardComponent;
  let fixture: ComponentFixture<BrowserCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BrowserCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BrowserCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
