import { Component, Input, OnInit } from '@angular/core';
import { Icon } from '../icon/icon.component';
import { SharedService } from '../shared.service';

@Component({
  selector: 'app-browser-card',
  templateUrl: './browser-card.component.html',
  styleUrls: ['./browser-card.component.scss'],
})
export class BrowserCardComponent implements OnInit {
  @Input() marginTop = '0';
  @Input() version = '0';
  @Input() icon: Icon = 'chrome';
  heading = 'Add to ';
  subheading = 'Minimum version ';
  isMobile: boolean = false;

  constructor(private sharedService: SharedService) {}

  ngOnInit(): void {
    this.isMobile = this.sharedService.isMobile;
    this.heading += this.icon.charAt(0).toUpperCase() + this.icon.substring(1);
    this.subheading += this.version;
  }
}
