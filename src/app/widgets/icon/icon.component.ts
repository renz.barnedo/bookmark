import { Component, Input, OnInit } from '@angular/core';

export type Icon =
  | 'chrome'
  | 'firefox'
  | 'opera'
  | 'arrow'
  | 'facebook'
  | 'twitter'
  | 'error'
  | 'hamburger'
  | 'close'
  | 'illustration-features-tab-1'
  | 'illustration-features-tab-2'
  | 'illustration-features-tab-3'
  | 'bg-dots'
  | 'illustration-hero';

@Component({
  selector: 'app-icon',
  templateUrl: './icon.component.html',
  styleUrls: ['./icon.component.scss'],
})
export class IconComponent implements OnInit {
  @Input() icon: Icon = 'chrome';

  constructor() {}

  ngOnInit(): void {}
}
