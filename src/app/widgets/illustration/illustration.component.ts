import { Component, Input, OnInit } from '@angular/core';
import { Icon } from '../icon/icon.component';

export type Direction = 'rtl' | 'ltr';

@Component({
  selector: 'app-illustration',
  templateUrl: './illustration.component.html',
  styleUrls: ['./illustration.component.scss'],
})
export class IllustrationComponent implements OnInit {
  @Input() direction: Direction = 'rtl';
  @Input() imageName: Icon = 'illustration-hero';

  constructor() {}

  ngOnInit(): void {}
}
