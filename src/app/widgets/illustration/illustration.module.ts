import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IllustrationComponent } from './illustration.component';
import { IconModule } from '../icon/icon.module';

@NgModule({
  declarations: [IllustrationComponent],
  imports: [CommonModule, IconModule],
  exports: [IllustrationComponent],
})
export class IllustrationModule {}
