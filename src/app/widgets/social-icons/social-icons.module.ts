import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SocialIconsComponent } from './social-icons.component';
import { IconModule } from '../icon/icon.module';

@NgModule({
  declarations: [SocialIconsComponent],
  imports: [CommonModule, IconModule],
  exports: [SocialIconsComponent],
})
export class SocialIconsModule {}
