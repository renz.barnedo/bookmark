import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SharedService {
  isMobile = window.innerWidth <= 768;

  constructor() { }
}
